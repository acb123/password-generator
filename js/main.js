function getPassword(){
    var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+?><:{}[]";
    var passwordlength = 12;
    var password = "";
    for (var i = 0; i < passwordlength; i++){
        var randomPassword = Math.floor(Math.random() * chars.length);
        password += chars.substring(randomPassword, randomPassword + 1);
    }
    document.getElementById("gen-password").value = password;
}
